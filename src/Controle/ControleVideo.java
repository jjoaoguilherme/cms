package Controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Modelo.Video;

public class ControleVideo {
		public boolean inserirVideo(Video vi) {
			boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "INSERT INTO video(titulo,vd) VALUES(?,?);";
				PreparedStatement ps = con.prepareStatement(sql);
				
				ps.setString(1, vi.getTitulo());
				ps.setString(2, vi.getVideo());
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		public ArrayList<Video> consultarVideo(){
			ArrayList<Video> lista = new ArrayList<Video>();
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "SELECT * FROM video;";
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				
				if(rs != null) {
					while(rs.next()) {
						Video vi = new Video();
						vi.setId(rs.getInt("id"));
						vi.setTitulo(rs.getString("titulo"));
						vi.setVideo(rs.getString("vd"));
						lista.add(vi);
					}
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return lista;
		}
		
		public boolean atualizarVideo(Video vi,int id) {
			boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "UPDATE video SET  titulo=?,vd=? WHERE id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, vi.getTitulo());
				ps.setString(2, vi.getVideo());
				ps.setInt(3, id);
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		
		public boolean deletarVideo(int id) {
			boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "DELETE FROM video WHERE id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setInt(1,id);
				if(!ps.execute()) {
					resultado = true;
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		

}
