package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Imagem;

public class ControleImagem {
	public boolean inserirImagem(Imagem im) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO imagem(titulo,img) VALUES(?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, im.getTitulo());
			ps.setBytes(2, im.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Imagem> consultarImagem(){
		ArrayList<Imagem> lista = new ArrayList<Imagem>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM imagem;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			if(rs != null) {
				while(rs.next()) {
					Imagem im = new Imagem();
					im.setId(rs.getInt("id"));
					im.setTitulo(rs.getString("titulo"));
					im.setImagem(rs.getString("img"));
					lista.add(im);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	
	public boolean atualizarImagem(Imagem im,int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE imagem SET  titulo=?,img=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, im.getTitulo());
			ps.setString(2, im.getImagem());
			ps.setInt(3, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean deletarImagem(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM imagem WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	

}
