package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Texto;

public class ControleTexto {
	public boolean inserirTexto(Texto tex) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO texto(titulo,text) VALUES(?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, tex.getTitulo());
			ps.setString(2, tex.getTexto());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Texto> consultarTexto(){
		ArrayList<Texto> lista = new ArrayList<Texto>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM texto;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			if(rs != null) {
				while(rs.next()) {
					Texto tex = new Texto();
					tex.setId(rs.getInt("id"));
					tex.setTitulo(rs.getString("titulo"));
					tex.setTexto(rs.getString("text"));
					lista.add(tex);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	
	public boolean atualizarTexto(Texto tex,int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE texto SET  titulo=?,text=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, tex.getTitulo());
			ps.setString(2, tex.getTexto());
			ps.setInt(3, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean deletarTexto(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM texto WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	

}
